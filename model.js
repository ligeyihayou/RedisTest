/**
 * Created by zyn on 2018/9/5.
 */
var redis = require("redis"),
    config = require("./config"),
    client = redis.createClient(config.Redis.port, config.Redis.host);

// 监听redis错误消息
client.on("error", function (error) {
    console.log(error);
});

// 输入redis的密码
client.auth(config.Redis.auth, function () {
    console.log('密码验证成功');
});


// 连接成功的回调函数
client.on('connect', function (err) {
    if (!err) {
        console.log('--connect--->>', !err)
    }
});


// 创建数据模型
// 需要为每个关注的人和粉丝创建不同的redis集合，其中id作为键的一部分：
// user:<id>: follows  关注
// user:<id>: followes  粉丝
//

module.exports = User;

function User(id, data) {
    this.id = id;
    this.data = data;
}

// 提供一个静态的查询方法
User.find = function (id, fn) {
    client.hgetall('user:' + id, function (err, obj) {
        if (err) return fn(err);
        fn(null, new User(id, obj));
    })
}

User.prototype.save = function (fn) {
    if (!this.id) {
        this.id = (Math.random()).toString().substr(3);
    }
    client.hmset('user:' + this.id + ':data', this.data, fn);
};
// multi相当于创建了一个 执行队列 多个执行的结果放到一个事物里面来处理
User.prototype.follow = function (user_id, fn) {
    client.multi().srem()
        .sadd('user:' + user_id + ':followers', this.id)
        .sadd('user:' + this.id + ':follows', user_id)
        .exec(fn)

};
// 取消关注
User.prototype.follow = function (user_id, fn) {
    client.multi().srem()
        .srem('user:' + user_id + ':followers', this.id)
        .srem('user:' + this.id + ':follows', user_id)
        .exec(fn)

};
// 获取所有关注的信息
User.prototype.getFollows = function (fn) {
    client.smembers('user:' + this.id + ':follows', fn);
};
// 获取所有粉丝的信息
User.prototype.getFollowers = function (fn) {
    client.smembers('user:' + this.id + ':followers', fn);
};
// 获取两个数据的交集

User.prototype.getFriends = function () {
    client.sinter('user:' + this.id + ':follows', 'user:' + this.id + ':followers');
};




