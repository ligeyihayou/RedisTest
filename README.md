// redis 支持的数据类型
	# 字符串(string)
	# 列表(list)
	# 数据集(set)
	# 哈希(hash)
	# 有序数据集(sorted hash)


	Tips：Redis和MongoDB最大的不同是，Redis存储的文档是扁平的数据，不支持多层的数据嵌套。
		  Redis存储的位置是内存之中，所以速度是很快的。（需要数据持久化的思路）

	// set 将一个字符串赋值给一个键值
	set my.key test  // ok

	// get 获取 使用set 设置的key的值
	get my.key  // test

	///  绝大部分的redis指令都依赖于数据类型，使用get和set都可以操作字符串，但是get 不能获取hash类型的值。

	// 除了使用get和set方式的时候还可以使用  INCR 将key值对应的value值进行递增
			  
	Hash  可以理解为JSON 类型的字符串，不能进行多层次的嵌套

	   // 操作hash的命令
	   		HSET profile.1 name Guilermo  // 设置hash值
	   		HGETALL profile.1  // 获取hash值
	   		HDEL profile.1 name //（这里的name 是 set时候的key）
	   		HEXISTS profile.1 name //查看name字段是否存在
    List （redis 中的列表等同于js中的数组）
    	RPUSH（push到右侧，从列表尾部添加数据）
    	LPUSH （push到左侧,从数组的头部添加）
    	RPUSH profile.1.josb "jobs11" // OK
    	// 获取指定返回的数组
    	LRANGE profile.1.josb 0 4 // 查看全部   0 -1 就可以
     
       
    SMEMBERS // 返回key中所有的成员   
    数据集(set)
    	// 数据集位于list和hash之间,拥有hash的属性(数据集中的每一项不能重复)
    	Redis允许在数据集,联合，获取到随机元素之间的做交集
    	SADD myset "key1"  // 设置数据
     有序的数据集合(高级用法)	
     
     multi // redis 里面打包执行多个命令
       
     client.multi([commands]) / 标志着一个事务的开始
     client.Multi.exec( callback ) // 开始执行事务
     
     
     demo
      // 进行模块的依赖
      var redis = require("redis");
      var  client = redis.createClient(config.Redis.port, config.Redis.host); // 如果是本地的redis 就不需要添加 端口号还有主机名称了，远程的话需要添加对应的连接信息
      client.auth(111, function() {}); // 这是一种设置密码的方式，还可以通过 createClient 第三个参数 redis.createClient(RDS_PORT,RDS_HOST,{auth_pass:111});
      
      client.on('connect', function() {
        // 客户端触发connect同时它会发出ready，如果设置了client.options.no_ready_check，当这个stream被连接时会触发connect
      })
      
      client.on('ready', function() {
        console.log('---redis服务器连接成功, 此时表示已经准备好接收命令-->>')
       })
      
      // 设置字符串数据 其他方式的使用方式相同
      client.set('mykey', 'myvalue', function(err) {
      })
      
      
      
      
      
     ///// 使用redis时候的需要注意的地方
     
     在停止redis的时候,需要考虑到可能正在将内存中的数据同步到硬盘之后，这样之后就会大致数据的丢失，正确的方式是通过 redis-cli shutdown (通过kill结束redis效果和shutdown一样的)
     
     /// redis.conf 配置文件，但是redis还提供了一套 在不需要重新启动的情况下修改redis的配置，就是(ConfIG GET/SET commed)'
     
     
     
     // redis入门指南 记录
     
     1. redis的基础命令
         KEYS * // 查看当前数据库的 所有key值 当数据量很大的时候不建议使用
         
         SET bar 1  // key为bar value是 1 设置字符串
         
         GET bar  // 获取Key bar对应的value的值
         
         EXISTS my.key // 查询当前的key值是否存在 1 存在， 0 不存在
         
         DEL my.key // 删除 key是my.key的数据记录 (不支持通配符)
         
         APPEND key value // 想key 所对应的value后面追加内容
         
         STRLEN key // 获取字符串的长度
          
          
         Redis持久化的两种方式
            1. RDB方式 (会根据指定的规则定时将数据回写到硬盘之上)
                RDB的方式是通过快照来完成的，当符合一定条件的时候redis会自动将内存中的数据生成一份副本保存到硬盘上面去的，这个过程称之为，快照。
                /// redis 会在一下这几种情况之下产生快照
                    1.1 根据配置的规则自动进行快照
                    1.2 用于调用 SAVE 或 BGSAVE 命令
                    1.3 执行 FLUSHALL 命令
                    1.4 在执行复制的时候。
                    
                    // 修改配置文件 redis.conf
                        save 900 1   15分钟(900s)有一个以上的key值改变了就进行快照
                        save 300 10
                        save 60 10000
                    // 手动执行 SAVE 或者 BGSAVE
                        // SAVE 在执行的时候会进行快照，这个时候会阻塞k客户端的请求，当数据库中的数据比较多得时候会导致redis长时间不能响应的，尽量不能再生产华景使用这个方法
                        // BGSAVE 在执行的时候进行快照，这个时候是异步的不会阻塞客户的请求，自动快照的时候采用的就是异步的
                       当服务进行重启的时候，手动迁移或者备份的时候，可进行手动备份
                    // 设置了主从模式的时候redis 会在复制初始化的时候执行快照，即使没有定义快照条件也会进行快照   
            2. AOF方式 (在每次之后将命令记录下来)
                // 当使用redis来存储非临时数据的时候，一般需要打开AOF来实现持久化，通过appendonly yes 来实现
            // 两种方式可以使用其中一种来实现，更多的是使用两种相结合的方式
     