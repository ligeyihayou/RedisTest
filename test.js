/**
 * Created by zyn on 2018/9/6.
 */
var User = require('./model');

// 测试用户数据

var testUser = {
    'liubei@qq.com': {name: 'liubei', age: 23},
    'guanyu@qq.com': {name: 'guanyu', age: 230},
    'zhangfei@qq.com': {name: 'zhangfei', age: 83},
    'lisi@qq.com': {name: 'lisi', age: 3},
    'pikaqiu@qq.com': {name: 'pikaqiu', age: 93}
}

function create(Users, fn) {
    // 获取数据的长度
    var total = Object.keys(Users).length;
    for (item in Users) {
        (function (email, data) {
            var user = new User(email, data);
            user.save(function (err) {
                if(err) throw err;
                --total || fn();
            });
        })(item, Users[item])
    }
}


create(testUser, function () {
    console.log('all user created');
})
